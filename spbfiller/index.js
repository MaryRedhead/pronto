 class Observer {
  constructor () {
    this.listeners = []
  }

  subscribe (fn) {
    this.listeners.push(fn)
  }

  unsubscribe (fn) {
    this.listeners = this.listeners.filter(subscriber => subscriber !== fn)
  }

  publish (data) {
    this.listeners.forEach(subscriber => subscriber(data))
  }
}
const COLORS = [
    '#eb4034', 
    '#16f546', 
    '#fffb1f', 
    '#0e0af2', 
    '#fc0aec',
    '#0afce4',
    '#fc8f0a',
    '#16f116', 
    '#fffaaf', 
    '#044af2' ];

const elements = [
     { id: 0, width: 100, height: 100, x: 10, y: 10, backgroundColor: '#5FD0E4' },
     { id: 1, width: 40, height: 100, x: 100, y: 200, backgroundColor: '#9E5FE4' }
]

const dragObjArr = [];
const container = document.getElementById('main');
const table = document.getElementById('table');
const hDiv = document.getElementById('heightDiv');
const wDiv = document.getElementById('widthDiv');
const labelX = document.getElementById('coordx');
const labelY = document.getElementById('coordy');
const observer = new Observer()

observer.subscribe((data) => {
    table.getElementsByClassName(`${data.id}X`)[0].value = data.x;
    table.getElementsByClassName(`${data.id}Y`)[0].value = data.y;
});


table.addEventListener("input", function (e) {
  const input = e.target;
  if (input.tagName === "INPUT") {
    const tr = input.parentElement.parentElement;
    const idDiv = +tr.querySelector(".id").textContent;
    const x = tr.querySelector(".x").children[0].value;
    const y = tr.querySelector(".y").children[0].value;
    const changedDiv = dragObjArr.find((item)=> {
        return item.element.id === `dragDiv${idDiv}`;
    });
    changedDiv.set(x,y);
    setElemOptions(idDiv, x, y);
  }
});

function setElemOptions(id, x, y) {
    const index = elements.findIndex((elem)=> {return elem.id ===id});
    elements[index].x=x;
    elements[index].y = y;
}


function createDragDiv(item) {
        let divElem = document.createElement('div');
        divElem.id=`dragDiv${item.id}`;
        divElem.style=`width: ${item.width}px; height: ${item.height}px; background: ${item.backgroundColor}`;
        container.appendChild(divElem);
        
        const trEl = document.createElement('tr');
        trEl.innerHTML = `<tr><td class='id'>${item.id}<td>${item.width}<td>${item.height}<td class='x'><input class='${item.id}X' type="number" value= ${item.x}><td class='y'><input class='${item.id}Y' type="number" value = ${item.y}><td>${item.backgroundColor}<td></tr>`;
        table.appendChild(trEl);

        const dragElem = new Draggable(divElem, 
            {
                id: item.id,
                limit: container,
                setCursor: true,
	            onDrag: function (element, x, y) {
	            	labelX.innerHTML = `X:${x}`;
                    labelY.innerHTML = `Y:${y}`;
                },
                onDragEnd: function(element, x, y) {
                    setElemOptions(item.id, x, y);
                    observer.publish({id:item.id, x, y})
                }
            });
        dragElem.set(item.x, item.y);
        dragObjArr.push(dragElem);
}

function onAddClick() {
    const newElem = {
        id: elements.length,
        width: wDiv.value,
        height: hDiv.value,
        x: Math.floor(Math.random() * 100),
        y: Math.floor(Math.random() * 100),
        backgroundColor: COLORS[Math.floor(Math.random() * 10)]
    }
    elements.push(newElem);
    createDragDiv(newElem)
   
}

function onPageLoad() {
   const divArr = elements.map((item) => {
        createDragDiv(item);
    });
}

document.addEventListener("DOMContentLoaded", onPageLoad);